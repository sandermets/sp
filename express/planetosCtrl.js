const fetch = require('node-fetch');

function fetchWeather(req) {
  const url = 'http://api.planetos.com/v1/datasets/bom_access-g_global_40km/point' +
    `?lon=${req.query.lon}` +
    `&lat=${req.query.lat}` +
    `&var=${req.query.var}` +
    `&start=${req.query.start}` +
    `&end=${req.query.end}` +
    `&count=${req.query.count}` +
    '&apikey=180b379e275f4e25bad13ae5578a2164' +
    '&json=true';

  return fetch(url)
    .then(response => response.json())
    .then(json => ({ success: true, results: json }))
    .catch(error => ({ success: false, error }));
}

module.exports = {
  getWeather: (req, res) => fetchWeather(req)
    .then(result => res.json(result))
    .catch(error => res.json({ success: false, error })),
};

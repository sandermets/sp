const express = require('express');

const router = express.Router();
const pvoutputCtrl = require('./pvoutputCtrl');
const planetosCtrl = require('./planetosCtrl');

router.route('/systems').get(pvoutputCtrl.getSystems);
router.route('/weather').get(planetosCtrl.getWeather);

module.exports = router;

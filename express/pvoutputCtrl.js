const Promise = require('bluebird');
const fetch = require('node-fetch');
const pvoutputRes = require('./pvoutputResponse.json');

const systems = {
  50283: 'Roessel I/II',
  49398: 'Biestraat',
  47697: 'Rimpelaar',
  54287: 'Nico de Witt',
  54493: 'Abbeelen',
  49567: 'Egelsbroek',
  46142: 'Nieuwelijn',
  47624: 'Radiotherapiegroep Arnhem',
  52684: 'Vijfhuizenbaan',
  51185: 'Belogroep',
  44894: 'Schipper Power - Two',
  52301: 'Zonpark Vierverlaten - Inv. 1',
  24763: 'Carl Siegert Bakery Harmelen',
  22059: 'Farm Jansen',
  52890: 'Hoevenseweg',
  46914: 'Versteeg Buurman SolarEdge',
  49339: 'Zweefvliegveld Malden',
  39089: 'Iewan',
  52484: 'deverfkampioen',
  51468: 'Woonborg- Helterbult',
  48274: 'Eekhoutswijk 4 Smilde',
  50052: 'PV_Polderploer',
  47281: 'Camunit',
  51758: 'Zonnedikker1',
  44038: 'GKVm',
  53123: 'De van Kesterenetjes  Totaal',
  41986: 'VDS test',
  49597: 'ZonInDrachten',
  53479: 'Zonnepanelen Oppedijk',
  49546: 'Symo 7.0-3-M',
  // 41005: 'Paulus',
};

/**
 * Date yyyymmdd date 20100830 r1
 * Time hh:mm time 14:10 r1
 * Energy Generation number watt hours 12936 r1
 * Power Generation number watt 202 r1
 * Energy Consumption number watt hours 19832 r1
 * Power Consumption number watt 459 r1
 * Normalised Output number kW/kW 0.083 r1
 * Temperature decimal celsius 15.3 r2
 * Voltage decimal volts 240.1
 * - getstatus.jsp - https://pvoutput.org/help.html
 */
const systemTemplate = {
  date: 'NaN',
  time: 'NaN',
  energyGeneration: 'NaN',
  powerGeneration: 'NaN',
  energyConsumption: 'NaN',
  powerConsumption: 'NaN',
  normalisedOutput: 'NaN',
  temperature: 'NaN',
  voltage: 'NaN',
};

function getSystemById(id) {
  return fetch(
    `https://pvoutput.org/service/r2/getstatus.jsp?sid1=${id}`,
    {
      method: 'GET',
      headers: {
        'X-Pvoutput-Apikey': '12993d501472095ba592b92e4497164beba09c4f',
        'X-Pvoutput-SystemId': '56565',
      },
    },
  )
    .then((response) => {
      return response.text();
    })
    .then((text) => {
      const result = Object.assign({}, systemTemplate);
      result.id = id;
      result.name = systems[id];
      const responseArray = text.split(',');
      if (responseArray.length !== 9) {
        return { success: false, result };
      }
      Object.keys(systemTemplate).forEach((val, i) => {
        result[val] = responseArray[i];
      });
      return { success: true, result };
    })
    .catch((error) => {
      console.log(error);
      return { success: false, error };
    });
}

module.exports = {
  getSystems: (req, res) => Promise
    .map(Object.keys(systems), getSystemById, { concurrency: 5 })
    .then((results) => {
      let success = true;
      results.forEach((result) => {
        success = success && result.success;
      });
      return res.json({ success, results });
    })
    .catch((error) => {
      console.log(error);
      // for presentational purposes going to use fake data if api limit reached
      // Note: comments in NodeJS make node runtime actually slower- https://medium.com/the-node-js-collection/get-ready-a-new-v8-is-coming-node-js-performance-is-changing-46a63d6da4de
      return res.json(pvoutputRes);
      // for real usage -> return res.json({ success: false, error });
    }),
};

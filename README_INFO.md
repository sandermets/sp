# solar-panels

## Run

### Brief

A dashboard to monitor solar farm energy production.

Solar farms consist of 30 solar panels, has a server that is able to provide voltage and wattage
per solar panel.

REST forecast data service is provided separately.

Requirements:
1. Dashboard has to tell the user what is the total energy in kW produced at the moment
(aggregated from all panels).
2. Dashboard has to list all panels with their IDs in a grid, where each cell has values of
current voltage and wattage output.
3. The dashboard should indicate basic weather forecast information like solar activity
"Visible Diffuse Downward Solar Flux" in watts per square metre and % of sky cloud
coverage (100% — clear sky).
4. The application UI should have a responsive layout, adapted to desktop, tablet and
smartphone screens.
5. The dashboard should be able to poll a server every 10 sec. to update voltage, wattage
information. And poll REST weather data service every 5 min.

It's ok to:
- generate fake data
- use CSS/JS frameworks

Nice to have:
1. Fit all 30 panels cells and weather forecast on a desktop size screen (1280px wide)
without scrolling.
2. Use real data from APIs.
3. Use React.js for rendering and Redux/Reflux for data flow
4. Use simple graphs for forecast visualisation (next 24h with hourly resolution)
APIs and sample data examples:
1. Cloud coverage

av_ttl_cld

http://api.planetos.com/v1/datasets/bom_access-g_global_40km/po
int?lon=51.8&lat=4.8&apikey=180b379e275f4e25bad13ae5578a2164&var=av_ttl_cld&json=true&
count=50

2. Solar activity

av_swsfcdown

http://api.planetos.com/v1/datasets/bom_access-g_global_40km/po
int?lat=51.8&lon=4.8&apikey=180b379e275f4e25bad13ae5578a2164&var=av_swsfcdown&json=tru
e&count=50

see details about this API in the docs http://docs.planetos.com/#datasets-id-point

3. Sample data for fake data generator could be derived from service like this one

http://pvoutput.org/live.jsp

## Workflow

Going to use real data

- choose ids of 30 NL systems from pvoutput
- use express/node to bypass Cross Domain Policy
- get data for solar panels for front end
- get weather data from planetOs for front end
- build UI

Systems handpicked from pvoutput:

50283 Roessel I/II  
49398 Biestraat  
47697 Rimpelaar  
54287 Nico de Witt  
54493 Abbeelen  
49567 Egelsbroek  
46142 Nieuwelijn  
47624 Radiotherapiegroep Arnhem  
52684 Vijfhuizenbaan  
51185 Belogroep  
44894 Schipper Power - Two  
52301 Zonpark Vierverlaten - Inv. 1  
24763 Carl Siegert Bakery Harmelen  
22059 Farm Jansen  
52890 Hoevenseweg  
46914 Versteeg Buurman SolarEdge  
49339 Zweefvliegveld Malden  
39089 Iewan  
52484 deverfkampioen  
51468 Woonborg- Helterbult  
49339 Zweefvliegveld Malden  
48274 Eekhoutswijk 4 Smilde  
50052 PV_Polderploer  
47281 Camunit  
51758 Zonnedikker1  
44038 GKVm  
53123 De van Kesterenetjes Totaal  
41986 VDS test  
49597 ZonInDrachten  
53479 Zonnepanelen Oppedijk  
49546 Symo 7.0-3-M  
41005 Paulus  

Lat
51.86292391

Long
4.83398438

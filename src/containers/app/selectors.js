import { createSelector } from 'reselect';

export const selectApp = state => state.get('app');

export const makeSelectPanels = () => createSelector(
  selectApp,
  app => app.get('panels').toJS(),
);

export const makeIsInitialLoading = () => createSelector(
  selectApp,
  app => app.get('isInitialLoading'),
);

export const makeSelectClouds = () => createSelector(
  selectApp,
  app => app.get('clouds').toJS(),
);

export const makeSelectSun = () => createSelector(
  selectApp,
  app => app.get('sun').toJS(),
);

export const makeSelectTotalPower = () => createSelector(
  selectApp,
  app => app.get('totalPower'),
);

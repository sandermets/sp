const API = {};

let url = '';
if (process.env.NODE_ENV === 'development') {
  url = 'http://localhost:3333';
}

async function getSystems() {
  try {
    const response = await window.fetch(
      `${url}/api/systems`,
      {
        method: 'GET',
        headers: {
          Accept: 'application/json',
          mode: 'cors',
          credentials: 'include',
          'Content-Type': 'application/json',
        },
      },
    );
    const results = await response.json();
    return { success: true, results };
  } catch (error) {
    return { success: false, error };
  }
}

API.getSystems = getSystems;

/**
 * theVar is var in API
 * in this tasks:
 * av_ttl_cld - Clouds
 * OR
 * av_swsfcdown - Sun
 * @param {*} theVar
 */
async function getWeather(theVar) {
  try {
    const startDateTime = new Date();
    startDateTime.setMinutes(0);
    startDateTime.setSeconds(0);
    const endDateTime = new Date();
    endDateTime.setHours(endDateTime.getHours() + 24);
    endDateTime.setMinutes(0);
    endDateTime.setSeconds(0);

    const response = await window.fetch(
      `${url}/api/weather` +
      '?lon=51.8' +
      '&lat=4.8' +
      `&var=${theVar}` +
      `&start=${startDateTime.toISOString()}` +
      `&end=${endDateTime.toISOString()}` +
      '&count=10',
      {
        method: 'GET',
        headers: {
          Accept: 'application/json',
          mode: 'cors',
          credentials: 'include',
          'Content-Type': 'application/json',
        },
      },
    );
    const results = await response.json();
    if (
      results.results.entries instanceof Array &&
      results.results.entries.length
    ) {
      return { success: true, results };
    }
    return { success: false, results };
  } catch (error) {
    return { success: false, error };
  }
}

API.getWeather = getWeather;

export default API;

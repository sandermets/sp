import * as actionType from './actionTypes';

export function fetchSolarPanelsAsync() {
  return {
    type: actionType.FETCH_SOLARPANELS_ASYNC,
  };
}

export function fetchSolarPanelsSuccess(results, totalPower) {
  return {
    type: actionType.FETCH_SOLARPANELS_SUCCESS,
    payload: { data: results, totalPower },
  };
}

export function fetchSolarPanelsFailure(error) {
  return {
    type: actionType.FETCH_SOLARPANELS_FAILURE,
    payload: { error },
  };
}

export function fetchCloudsAsync() {
  return {
    type: actionType.FETCH_CLOUDS_ASYNC,
  };
}

export function fetchCloudsSuccess(clouds) {
  return {
    type: actionType.FETCH_CLOUDS_SUCCESS,
    payload: { clouds },
  };
}

export function fetchCloudsFailure(error) {
  return {
    type: actionType.FETCH_CLOUDS_FAILURE,
    payload: { error },
  };
}

export function fetchSunAsync() {
  return {
    type: actionType.FETCH_SUN_ASYNC,
  };
}

export function fetchSunSuccess(sun) {
  return {
    type: actionType.FETCH_SUN_SUCCESS,
    payload: { sun },
  };
}

export function fetchSunFailure(error) {
  return {
    type: actionType.FETCH_SUN_FAILURE,
    payload: { error },
  };
}

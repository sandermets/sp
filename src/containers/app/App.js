/* eslint-disable no-restricted-globals */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Loader, Segment, Label, Message } from 'semantic-ui-react';
import WeatherChart from './WeatherChart';
import detectPanelLabelColor from './detectPanelLabelColor';
import './App.css';
import {
  AppContainer,
  LayoutBox,
  LoaderBox,
  PanelsBox,
  PanelBox,
  SegmentBox,
  TotalPowerBox,
  PanelMetricsBox,
  PanelMetricText,
  ChartsBox,
  ChartBox,
} from './styles';

class App extends Component {
  componentDidMount() {
    this.props.fetchSolarPanels();
    this.props.fetchClouds();
    this.props.fetchSun();
  }

  render() {
    const {
      panels,
      isInitialLoading,
      clouds,
      sun,
      totalPower,
    } = this.props;

    return (
      <AppContainer>
        <LayoutBox style={{ maxWidth: '1278px' }}>

          {isInitialLoading &&
            <LoaderBox>
              <Loader active inline="centered" />
            </LoaderBox>
          }

          {!isInitialLoading && !panels.length &&
            <Message>
              <Message.Header>
                No data
              </Message.Header>
              <p>
                We could not receive data for solar panels :(
              </p>
            </Message>
          }

          {!isInitialLoading && !!panels.length &&
            <PanelsBox>
              <PanelBox >
                <SegmentBox raised>
                  <Label as="a" color="black" ribbon>
                    Total energy produced at the moment
                  </Label>
                  <TotalPowerBox>
                    <PanelMetricText {...{ color: '#000', fontWeight: 'bold' }}>
                      {totalPower} kW
                    </PanelMetricText>
                  </TotalPowerBox>
                </SegmentBox>
              </PanelBox>
              {
                panels.map(panel => (
                  <PanelBox key={panel.result.id} >
                    <Segment raised textAlign="right">
                      <Label as="a" color={detectPanelLabelColor(panel.result.powerGeneration, panel.result.voltage)} ribbon>{panel.result.id}</Label>
                      <PanelMetricsBox>

                        <PanelMetricText
                          color={(isNaN(panel.result.voltage)) ? 'red' : '#000'}
                          fontWeight={(isNaN(panel.result.voltage)) ? 'normal' : 'bold'}
                        >
                          {(isNaN(panel.result.voltage)) ? 'N/A' : panel.result.voltage} V
                        </PanelMetricText>

                        <PanelMetricText
                          color={(isNaN(panel.result.powerGeneration)) ? 'red' : '#000'}
                          fontWeight={(isNaN(panel.result.powerGeneration)) ? 'normal' : 'bold'}
                        >
                          {(isNaN(panel.result.powerGeneration)) ? 'N/A' : panel.result.powerGeneration} W
                        </PanelMetricText>

                      </PanelMetricsBox>
                    </Segment>
                  </PanelBox>
                ))
              }
            </PanelsBox>
          }

          {!isInitialLoading &&
            <ChartsBox>
              <ChartBox>
                {
                  !isInitialLoading && !!Object.keys(clouds).length &&
                  <Message>
                    <Message.Header>
                      Percentage of sky cloud coverage (100 — clear sky)
                    </Message.Header>
                    {
                      !!clouds.chart &&
                      clouds.chart.values instanceof Array &&
                      !!clouds.chart.values.length &&
                      <WeatherChart {...{ data: clouds }} />
                    }
                  </Message>
                }
              </ChartBox>
              <ChartBox>
                {
                  !isInitialLoading && !!Object.keys(sun).length &&
                  <Message>
                    <Message.Header>
                      Visible Diffuse Downward Solar Flux [W/&#13217;]
                    </Message.Header>
                    {
                      !!sun.chart &&
                      sun.chart.values instanceof Array &&
                      !!sun.chart.values.length &&
                      <WeatherChart {...{ data: sun }} />
                    }
                  </Message>
                }
              </ChartBox>
            </ChartsBox>
          }
        </LayoutBox>
      </AppContainer>
    );
  }
}

App.propTypes = {
  panels: PropTypes.array,
  fetchSolarPanels: PropTypes.func,
  fetchClouds: PropTypes.func,
  fetchSun: PropTypes.func,
  isInitialLoading: PropTypes.bool,
  clouds: PropTypes.object,
  sun: PropTypes.object,
  totalPower: PropTypes.number,
};

export default App;

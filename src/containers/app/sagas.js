import { put, call, takeEvery } from 'redux-saga/effects';
import { delay } from 'redux-saga';

import * as actionType from './actionTypes';
import * as action from './actions';
import API from './api';

export function* fetchSolarPanelsAsync() {
  try {
    const {
      results,
      success,
      error,
    } = yield call(API.getSystems);
    if (success) {
      const totalPower = Math.round((
        results.results
          .filter(o => !isNaN(o.result.powerGeneration)) // eslint-disable-line no-restricted-globals, max-len
          .map(o => +o.result.powerGeneration)
          .reduce((accumulator, currentValue) => accumulator + currentValue)
        / 1000));
      yield put(action.fetchSolarPanelsSuccess(results, totalPower));
    } else {
      yield put(action.fetchSolarPanelsFailure(error));
    }
    yield delay(10 * 1000);
    yield put(action.fetchSolarPanelsAsync());
  } catch (error) {
    yield put(action.fetchSolarPanelsFailure(error));
    console.log(error);
    yield delay(10 * 1000);
    yield put(action.fetchSolarPanelsAsync());
  }
}

export function* fetchCloudsAsync() {
  try {
    const {
      results,
      success,
      error,
    } = yield call(API.getWeather, 'av_ttl_cld');
    if (success) {
      const cloudsRes = results.results.entries;
      const clouds = {
        chart: {
          label: '',
          values: [],
        },
      };
      clouds.values = [];
      const cloudsResKeysArr = Object.keys(cloudsRes);
      cloudsResKeysArr
        .forEach((key) => {
          const cloudsObj = {};
          cloudsObj.x = Date.parse(cloudsRes[key].axes.time);
          cloudsObj.y = Math.round(cloudsRes[key].data.av_ttl_cld * 100);
          clouds.chart.values.push(cloudsObj);
        });
      clouds.chart.values.sort((a, b) => (a.x - b.x));
      clouds.firstDateTime = Date.parse(cloudsRes[cloudsResKeysArr[0]].axes.time);
      clouds.lastDateTime = Date.parse(cloudsRes[cloudsResKeysArr[(cloudsResKeysArr.length - 1)]].axes.time); // eslint-disable-line max-len
      yield put(action.fetchCloudsSuccess(clouds));
    } else {
      yield put(action.fetchCloudsFailure(error));
    }
    yield delay(5 * 60 * 1000);
    yield put(action.fetchCloudsAsync());
  } catch (error) {
    yield put(action.fetchCloudsFailure(error));
    yield delay(5 * 60 * 1000);
    yield put(action.fetchCloudsAsync());
  }
}

export function* fetchSunAsync() {
  try {
    const {
      results,
      success,
      error,
    } = yield call(API.getWeather, 'av_swsfcdown');
    if (success) {
      const sunRes = results.results.entries;
      const sun = {
        chart: {
          label: '',
          values: [],
        },
      };
      sun.values = [];
      const cloudsResKeysArr = Object.keys(sunRes);
      cloudsResKeysArr
        .forEach((key) => {
          const sunObj = {};
          sunObj.x = Date.parse(sunRes[key].axes.time);
          sunObj.y = sunRes[key].data.av_swsfcdown;
          sun.chart.values.push(sunObj);
        });
      sun.chart.values.sort((a, b) => (a.x - b.x));
      sun.firstDateTime = Date.parse(sunRes[cloudsResKeysArr[0]].axes.time);
      sun.lastDateTime = Date.parse(sunRes[cloudsResKeysArr[(cloudsResKeysArr.length - 1)]].axes.time); // eslint-disable-line max-len
      yield put(action.fetchSunSuccess(sun));
    } else {
      yield put(action.fetchSunFailure(error));
    }
    yield delay(5 * 60 * 1000);
    yield put(action.fetchSunAsync());
  } catch (error) {
    yield put(action.fetchSunFailure(error));
    console.log(error);
    yield delay(5 * 60 * 1000);
    yield put(action.fetchSunAsync());
  }
}

export function* watchApp() {
  yield takeEvery(actionType.FETCH_SOLARPANELS_ASYNC, fetchSolarPanelsAsync);
  yield takeEvery(actionType.FETCH_CLOUDS_ASYNC, fetchCloudsAsync);
  yield takeEvery(actionType.FETCH_SUN_ASYNC, fetchSunAsync);
}

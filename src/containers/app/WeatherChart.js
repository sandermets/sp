import React from 'react';
import PropTypes from 'prop-types';
import { d3, LineChart } from 'react-d3-components';

const WeatherChart = ({ data }) => (
  <LineChart
    data={data.chart}
    width={window.innerWidth < 1108 ? 360 : 480}
    height={236}
    interpolate="basis"
    margin={{
      top: 10,
      bottom: 25,
      left: 35,
      right: 10,
    }}
    color="green"
    xScale={
      d3.time
        .scale()
        .domain([data.firstDateTime, data.lastDateTime])
        .range([0, 480 - 70])
    }
    xAxis={{
      tickValues: d3.time.scale()
        .domain([data.firstDateTime, data.lastDateTime])
        .range([0, 480 - 10]).ticks(d3.time.hour, 1),
      tickFormat: d3.time.format('%H'),
    }}
  />
);

WeatherChart.propTypes = {
  data: PropTypes.object,
};

export default WeatherChart;

import styled from 'styled-components';
import { Segment } from 'semantic-ui-react';

export const AppContainer = styled.div`
    padding: 2rem 1rem 1rem 1rem;
    width: 100%;
    display: flex;
    flex-flow: row wrap;
    align-content: center;
    justify-content: center;
`;

export const LayoutBox = styled.div`
    max-width: 1278px;
    display: flex;
    flex-flow: row wrap;
    align-content: center;
    justify-content: center;
`;

export const LoaderBox = styled.div`
  display: flex;
  flex-flow: row wrap;
  align-items: center;
`;

export const PanelsBox = styled.div`
  display: flex;
  flex-flow: row wrap;
  align-items: flex-start;
  word-wrap: break-word;
  align-content: space-between;
`;

export const PanelBox = styled.div`
  margin: -0.9rem -0.9rem 1.1rem 1.1rem;
`;

export const SegmentBox = styled(Segment)`
  min-width: 20.42rem;
`;

export const TotalPowerBox = styled.div`
  margin-top: 0.5rem;
  padding-top: 0.5rem;
  padding-bottom: 0.5rem;
  padding-right: 0.2rem;
  background-color: #fff;
  min-width: 1.2rem;
  font-weight: bold;
  color: #777;
  min-height: 4.56rem;
  text-align: center;
  font-size: xx-large;
  border-radius: 0.28571429rem;
  border: 1px solid rgba(34,36,38,.15);
`;

export const PanelMetricsBox = styled.div`
  margin-top: 0.5rem;
  padding-top: 0.22rem;
  padding-bottom: 0.5rem;
  padding-right: 0.2rem;
  background-color: #fff;
  min-width: 1.2rem;
  fontweight: bold;
  color: #000;
  border-radius: 0.28571429rem;
  border: 1px solid rgba(34,36,38,.15);
`;

export const PanelMetricText = styled.p`
  color: ${props => (props.color ? `${props.color}` : '#000')};
  font-weight: ${props => (props.fontWeight ? `${props.fontWeight}` : 'normal')};
`;

export const ChartsBox = styled.div`
  display: flex;
  flex-flow: row wrap;
`;

export const ChartBox = styled.div`
  margin: -0.5rem 0.5rem 0.5rem 0.5rem;
`;

export default function detectPanelLabelColor(powerGeneration, voltage) {
  if (powerGeneration > 0 && voltage > 0) {
    return 'green';
  } else if (powerGeneration > 0 || voltage > 0) {
    return 'yellow';
  }
  return 'red';
}

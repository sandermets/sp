import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import * as action from './actions';
import * as selector from './selectors';
import App from './App';

export const mapStateToProps = createStructuredSelector({
  panels: selector.makeSelectPanels(),
  isInitialLoading: selector.makeIsInitialLoading(),
  clouds: selector.makeSelectClouds(),
  sun: selector.makeSelectSun(),
  totalPower: selector.makeSelectTotalPower(),
});

export const mapDispatchToProps = dispatch => ({
  fetchSolarPanels: () => dispatch(action.fetchSolarPanelsAsync()),
  fetchClouds: () => dispatch(action.fetchCloudsAsync()),
  fetchSun: () => dispatch(action.fetchSunAsync()),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(App);

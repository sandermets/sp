import { fromJS } from 'immutable';
import * as actionTypes from './actionTypes';

const initialState = fromJS({
  isInitialLoading: true,
  panels: [],
  clouds: {},
  sun: {},
  totalPower: null,
});

const appReducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.FETCH_SOLARPANELS_SUCCESS: {
      return state.merge(fromJS({
        panels: action.payload.data.results,
        isInitialLoading: false,
        totalPower: action.payload.totalPower,
      }));
    }

    case actionTypes.FETCH_SOLARPANELS_FAILURE: {
      return state.merge(fromJS({
        isInitialLoading: false,
      }));
    }

    case actionTypes.FETCH_CLOUDS_SUCCESS: {
      const { clouds } = action.payload;
      return state.merge(fromJS({ clouds }));
    }

    case actionTypes.FETCH_SUN_SUCCESS: {
      try {
        const { sun } = action.payload;
        return state.merge(fromJS({ sun }));
      } catch (error) {
        return state;
      }
    }

    default:
      return state;
  }
};

export default appReducer;

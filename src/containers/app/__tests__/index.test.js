import React from 'react';
import ReactDOM from 'react-dom';
import renderer from 'react-test-renderer'; // eslint-disable-line import/no-extraneous-dependencies
import { mount } from 'enzyme';
import App from '../index';

describe('<App />', () => {
  it('renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<App />, div);
  });

  it('matches the snapshot', () => {
    const componentSnapShot = renderer
      .create(<App />)
      .toJSON();
    expect(componentSnapShot).toMatchSnapshot();
  });

  it('renders Title', () => {
    const wrapper = mount(<App />);
    expect(wrapper.find('.App-title').length).toEqual(1);
  });
});

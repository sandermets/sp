import { all } from 'redux-saga/effects';
import { watchApp } from './containers/app/sagas';

export default function* rootSaga() {
  yield all([
    watchApp(),
  ]);
}

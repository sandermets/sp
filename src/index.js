import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import 'semantic-ui-css/semantic.min.css';

import getStore from './store';
import './index.css';
import App from './containers/app';
import registerServiceWorker from './registerServiceWorker';

const Container = () => (
  <Provider store={getStore()}>
    <App />
  </Provider>
);

ReactDOM.render(<Container />, document.getElementById('root'));
registerServiceWorker();

import { combineReducers } from 'redux-immutable';
import appReducer from './containers/app/reducer';

export default function rootReducer() {
  return combineReducers({
    app: appReducer,
  });
}

const express = require('express');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const path = require('path');
const routerDefined = require('./express/routes');

const port = process.env.PORT || 3333;
const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());

if (process.env.NODE_ENV === 'development') {
  app.use((req, res, next) => {
    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', 'http://localhost:3000');
    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type,Authorization,x-access-token,mode,credentials');
    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);
    // Pass to next layer of middleware
    next();
  });
}

app.get('/health', (req, res) => res.json({ message: 'ok' }));
app.use('/api', routerDefined);
app.use(express.static(path.join(__dirname, '/build')));
app.get('*', (req, res) => res.sendFile(path.join(__dirname, '/public/index.html')));
app.listen(port);

console.log(`Successfully started! Env ${process.env.NODE_ENV} Listening on :${port}`);

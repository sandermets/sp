FROM node:8.9-alpine
ENV NODE_ENV production
ENV PORT 8888
WORKDIR /usr/src/app
COPY ["package.json", "./"]
COPY . .
RUN npm i -g yarn \
  && yarn install \
  && yarn build-css \
  && yarn build-js
EXPOSE 8888
CMD node server.js

# solar-panels

## Run

```sh
docker-compose -f docker-compose.yml up -d --build
```
- open [http://localhost:8888/](http://localhost:8888/)

## Develop

- node 8.9.4 recommended (latest LTS)

- before first develop run install dependencies
```sh
yarn install
```
- run express
```sh
NODE_ENV=development node server.js
```
- run react
```sh
yarn start
```
## System overview

## Solution

30 systems in Netherlands from pvoutput were selected by their ids.

Client shows output from real Solar systems.
For weather real PlanetOS API requests are being fired.

### Screenshots
1280x720:  
![alt text](./screenshots/1280x720.png "1280x720")

360x640:  
![alt text](./screenshots/360x640.png "1280x720")

### Client
Front end is React Redux. Architecture is following patterns from [react-boilerplate](https://github.com/react-boilerplate/react-boilerplate/blob/master/docs/general/introduction.md#the-hitchhikers-guide-to-react-boilerplate)

- React
- Redux
- Redux Saga
- Reselect
- ImmutableJS
- Styled Components

### Server

Node express creates requests targeting

- pvoutput.org API
- planetOS API

Because there are quite many requests for pvoutput (30 reqs at once after every 10s) and API limit is reached fast,
then fake data is being used for response if limits are exceeded.

## Conclusion (TODOs)

Time spent on this work was 4-5 in total, including API reasearch. Lot could be done to improve the solution.

- To add unit tests for client and server
- To add e2e tests (WebdriveIO or Nightmare) fro client
- To research more about planetOS API and confirm metrics av_ttl_cld and av_swsfcdown units
- Add cache for API requests in Express server
